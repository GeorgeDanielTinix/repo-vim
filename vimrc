<<<<<<< HEAD
" vim:et sw=2 ts=2 fdm=marker
" Author: Daniel Tinivella <dtinivella@gmail.com>
" GitHub: https://github.com/tinix
set rtp+=/home/tinix/anaconda3/lib/python3.7/site-packages/powerline/bindings/vim/

=======
set rtp+=/home/tinix/anaconda3/lib/python3.7/site-packages/powerline/bindings/vim/

let g:airline_theme='molokai' 
"mas info en .vim/bundle/vim-airline-theme/autoload/airline/theme
>>>>>>> c6f1d75d3d05f29104410185c98ed89a22ad170f
set laststatus=2
set t_Co=256
set showtabline=2
set noshowmode
set autoindent
set cindent
set number
<<<<<<< HEAD
set tabstop=2 
syntax enable
=======
"set colorcolumn=110
" line roja costado derecho :(
set tabstop=8 "ser for C programming"
colo default

syntax on
>>>>>>> c6f1d75d3d05f29104410185c98ed89a22ad170f
set expandtab
set mouse=a
set ruler
set cmdheight=2
set pastetoggle=<F9> "copiar y pegar
set nobackup
set noswapfile
"copiar y pegar con shift + ruedita del mouse o F9
set paste 
set clipboard=unnamed 

execute pathogen#infect()

"For vundle
filetype off
set rtp+=~/.vim/bundle/vundle
call vundle#rc()

" Dependency of snipmate, not sure what it is :/
Bundle "MarcWeber/vim-addon-mw-utils"
" Utility Funtions for other plugins, snipmate dependency
Bundle "tomtom/tlib_vim"
" Collection of snippet , snippet dependency
Bundle "honza/vim-snippets"
" Snipperts ofr our use :)
Bundle 'garbas/vim-snipmate'
Bundle 'tpope/vim-fugitive'
" Dependnecy managment
Bundle 'gmarik/vundle'
" Rails :/
Bundle 'tpope/vim-rails.git'
"Commenting and uncommenting stuf
Bundle 'tomtom/tcomment_vim' 
" Molokai theme
Bundle 'vim-ruby/vim-ruby'
" Surround your code :)
Bundle 'tpope/vim-surround'
"Parentesis autogeneraso
Bundle 'jiangmiao/auto-pairs'
" Genera blokes de codido comunes tab completations
 Bundle 'ervandew/supertab'
"Fuzzy finder for vim (Ctrl + P)
Bundle 'kien/ctrlp.vim'
"Distpatching the test runner to tmux pane
Bundle 'tpope/vim-dispatch'
"NERDTreeToogle
Bundle  'scrooloose/nerdtree'
" esperimental vundle
Bundle 'tpope/vim-vinegar'

<<<<<<< HEAD
=======

>>>>>>> c6f1d75d3d05f29104410185c98ed89a22ad170f
Bundle 'airblade/vim-gitgutter'
Bundle 'cakebaker/scss-syntax.vim'
Bundle 'craigemery/vim-autotag'
Bundle 'chriskempson/base16-vim'
Bundle 'danro/rename.vim'
Bundle 'edkolev/tmuxline.vim'
Bundle 'heartsentwined/vim-emblem'
Bundle 'kchmck/vim-coffee-script'
Bundle 'michaeljsmith/vim-indent-object'
Bundle 'rking/ag.vim'
Bundle 'sophacles/vim-bundle-mako'
Bundle 'terryma/vim-expand-region'
Bundle 'thoughtbot/vim-rspec'
Bundle 'tpope/vim-bundler'
Bundle 'tpope/vim-commentary'
Bundle 'tpope/vim-endwise'
Bundle 'tpope/vim-repeat'
Bundle 'tpope/vim-speeddating'
Bundle 'slim-template/vim-slim'
Bundle 'scrooloose/syntastic'
Bundle 'ngmy/vim-rubocop'
Bundle 'mustache/vim-mustache-handlebars'
Bundle 'vim-scripts/indenthtml.vim'
Bundle 'camthompson/vim-ember'
Bundle 'tpope/vim-unimpaired'
Bundle 'tpope/vim-rhubarb'
Bundle 'SirVer/ultisnips'
Bundle 'dhruvasagar/vim-table-mode'
Bundle 'godlygeek/tabular'
Bundle 'plasticboy/vim-markdown'
Bundle 'dyng/ctrlsf.vim' 
"for really nice search results like in sublime
Bundle 'yonchu/accelerated-smooth-scroll'
Bundle 'vim-scripts/IndexedSearch'
Bundle 'itspriddle/vim-jquery'
Bundle 'tpope/vim-haml'
<<<<<<< HEAD

=======
Bundle 'junegunn/goyo.vim'

"Ruby stuff: Thanks Ben :)
>>>>>>> c6f1d75d3d05f29104410185c98ed89a22ad170f
" ================
filetype plugin indent on " Enable filetype-specific indenting and plugins

augroup myfiletypes  
    " Clear old autocmds in group
    autocmd!
    " autoindent with two spaces, always expand tabs
    autocmd FileType ruby,eruby,yaml,markdown set ai sw=2 sts=2 et
augroup END  
" ================
" Config theme gruvbox dark hard
colorscheme gruvbox  " cool color scheme
let g:airline_theme='gruvbox'
:set background=dark
let g:gruvbox_contrast_dark = 'hard'
let g:gruvbox_termcolors = 256"
set cursorline!

" Show trailing whitespace and spaces before a tab:
:highlight ExtraWhitespace ctermbg=red guibg=red
:autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\\t/

"searching
set hlsearch
set incsearch
set ignorecase
set smartcase


" Tab completion
set wildmode=list:longest,list:full  
set wildignore+=*.o,*.obj,.git,*.rbc,*.class,.svn,vendor/gems/*


" let NERDTreeQuitOnOpen=1 , cuando se abre el archivo se esconde NERDTree
let NERDTreeQuitOnOpen=1
" utilizar la tecla para easy motion SPC
let mapleader=" "

"NERDTree abre y cierra
nmap <Leader>nt :NERDTreeFind<CR>  
nmap <Leader><F10> :GitGutterToggle<CR>
" save file with this config
nmap <Leader>w :w<CR>
" exit nvim whitout save 
nmap <Leader>q :q<CR>
" Refresh navbar nerdtree
nmap <Leader>r :NERDTreeFocus<CR>

"copiar y pegar con shift + ruedita del mouse o F9
set paste 


"Refresh to NERDTree con los nuevos files, con Ctrl + l para refresh
fun! ToggleNERDTreeWithRefresh()
    :NERDTreeToggle 
    if(exists("b:NERDTreeType") == 1)
        call feedkeys("R")  
    endif   
endf 

nmap <silent> <c-l> :call ToggleNERDTreeWithRefresh()<cr> 

Plugin 'morhetz/gruvbox' 
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'jpo/vim-railscasts-theme'
Plugin 'alvan/vim-closetag'
"Plugin Emmet
Plugin 'mattn/emmet-vim'
" plugin React and javascript
Plugin 'ekalinin/Dockerfile.vim'
Plugin 'loremipsum' 
 
" autoRefresh NERDTree para que no tenga que cerrar y abrir todo el tiempo
" autocmd CursorHold,CursorHoldI * call NERDTreeFocus() | call g:NERDTree.ForCurrentTab().getRoot().refresh() | call g:NERDTree.ForCurrentTab().render() | wincmd w

"NERDTRee mostrame los archivos ocultos
let NERDTreeShowHidden=1

"Config de javascript
let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_flow = 1
"set foldmethod=syntax "metodo de plegado, 

 " auto close tag 
let g:closetag_filenames = "*.html,*.xhtml,*.phtml,*.php, *.jsx"


" -- Autocompletation
filetype plugin on
set omnifunc=syntaxcomplete#Complete
autocmd FileType html set omnifunc=htmlcomplete#CompleteTag " autocomplete html

" AutoPairEnableMode
let g:AutoPairsFlyMode = 1

let g:ale_linters = {
      \   'ruby': ['standardrb', 'rubocop'],
      \   'python': ['flake8', 'pylint'],
      \   'javascript': ['eslint'],
      \}

let g:ale_fixers = {
      \    'ruby': ['standardrb'],
      \}
let g:ale_fix_on_save = 1

" auto complete ruby
let g:deoplete#enable_at_startup = 1

" auto comple con solargraph
" antes : gem install solargraph
let g:LanguageClient_serverCommands = {
    \ 'ruby': ['~/.rbenv/shims/solargraph', 'stdio'],
    \ }


